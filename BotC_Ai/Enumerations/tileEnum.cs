﻿/// <summary>
///     Enumeration for the tile type as specified by the standard.
/// </summary>
public enum tileEnum
{
    GRASS,
    START,
    CHECK_POINT,
    EYE,
    HOLE,
    RIVER,
    LEMBAS,
    EAGLE
}