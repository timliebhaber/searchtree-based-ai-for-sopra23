/// <summary>
///     Enumeration for the charcters as specified by the standard.
/// </summary>
public enum characterEnum
{
    FRODO,
    SAM,
    LEGOLAS,
    GIMLI,
    GANDALF,
    ARAGORN,
    GOLLUM,
    GALADRIEL,
    BOROMIR,
    BAUMBART,
    MERRY,
    PIPPIN,
    ARWEN
}