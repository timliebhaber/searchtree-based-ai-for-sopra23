/// <summary>
///     Enumeration for the messages as specified by the standard.
/// </summary>
public enum messageEnum
{
    ERROR,
    INVALID_MESSAGE,
    PARTICIPANTS_INFO,
    HELLO_SERVER,
    HELLO_CLIENT,
    GOODBY_SERVER,
    PLAYER_READY,
    GAME_START,
    GAME_END,
    RECONNECT,
    CHARACTER_OFFER,
    CHARACTER_CHOICE,
    CARD_OFFER,
    CARD_CHOICE,
    ROUND_START,
    CARD_EVENT,
    RIVER_EVENT,
    EAGLE_EVENT,
    SHOT_EVENT,
    GAME_STATE,
    PAUSE_REQUEST,
    PAUSED
}