/// <summary>
///     Enumeration for the roles as specified by the standard.
/// </summary>
public enum roleEnum
{
    PLAYER,
    SPECTATOR,
    AI
}