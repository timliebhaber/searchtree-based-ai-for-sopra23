/// <summary>
///     Enumeration for the directions as specified by the standard.
/// </summary>
public enum directionEnum

{
    NORTH,
    EAST,
    SOUTH,
    WEST
}