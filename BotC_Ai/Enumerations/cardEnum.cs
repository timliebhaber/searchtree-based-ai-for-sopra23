/// <summary>
///     Enumeration for the card types as specified by the standard.
/// </summary>
public enum cardEnum
{
    MOVE_3,
    MOVE_2,
    MOVE_1,
    MOVE_BACK,
    U_TURN,
    RIGHT_TURN,
    LEFT_TURN,
    AGAIN,
    LEMBAS,
    EMPTY
}